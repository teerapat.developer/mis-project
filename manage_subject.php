<!DOCTYPE html>
<html>
<head>
	<title>ระบบแต่งตั้งอาจารย์พิเศษ</title>
    <meta charset="UTF-8">

    <?php include 'lib/css_cdn.php';?>
    <link href="lib/manage_subject.css" rel="stylesheet">

</head>
<body>
	<div class="page-wrapper chiller-theme toggled">
		<a href="#" id="show-sidebar" class="btn btn-sm btn-dark" style="width: 100px; height: 50px">
			Console <br />
			<i class="fa fa-sliders"></i>
		</a>
		<nav id="sidebar" class="sidebar-wrapper">
			<div class="sidebar-content">
				<div class="sidebar-brand">
					<a>Console</a>
					<div id="close-sidebar">
						<i class="fa fa-close"></i>
					</div>
				</div>
				<div class="sidebar-header">
					<div class="user-pic">
						<img class="img-responsive img-rounded" src="https://raw.githubusercontent.com/azouaoui-med/pro-sidebar-template/gh-pages/src/img/user.jpg" alt="User picture">
					</div>
					<div class="user-info">
						<span id="adminName" class="user-name"></span>
						<span class="user-role">Administrator</span>
					</div>
				</div>

				<div class="sidebar-menu">
					<ul>
						<li class="header-menu">
							<span>Menu</span>
						</li>
						<li>
                            <a href="manage_teacher.php">
                                <i class="fa fa-graduation-cap"></i>
                                <span>อาจารย์</span>
                            </a>
                        </li>
                        <li>
                            <a href="manage_subject.php">
                                <i class="fa fa-book"></i>
                                <span>รายวิชา</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-inbox"></i>
                                <span>เลขที่คำสั่ง มทส.</span>
                            </a>
                        </li>
                        <li>
                            <a href="create_document.php">
                                <i class="fa fa-file"></i>
                                <span>ออกหนังสือแต่งตั้ง</span>
                            </a>
                        </li>
                        <li>
                            <a href="manage_user.php">
                                <i class="fa fa-user"></i>
                                <span>ผู้ใช้ระบบ</span>
                            </a>
                        </li>
					</ul>
				</div>
			</div>

			<div class="sidebar-footer">
				<a href="#" id="btn-logout">
					<i class="fa fa-power-off"></i>&nbsp;&nbsp;ออกจากระบบ
				</a>
			</div>
		</nav>

		<main class="page-content">
            <div id="main">
                <div class="card" id="card-title">
					<div class="card-body">
						<h2>
							<b style="margin-left: 10px;">รายวิชา</b>
							<i style="margin-left: 20px;" class="fa fa-book"></i>
						</h2>
					</div>
				</div>

				<div class="card" id="card-table">
					<div class="card-body">
						<table
							id="table"
							data-toggle="table"
							data-toolbar="#toolbar"
							data-search="true"
							data-show-refresh="true"
							data-page-list="[5, 10, 20, 100]"
							data-pagination="true"
							data-pagination-pre-text="Previous"
							data-pagination-next-text="Next"
							data-page-size="10"
							data-url="controller/get_data_allsubject.php">
							<thead class="thead-dark">
								<tr>
									<th data-width="100" data-field="subject_id" data-sortable="true">รหัสวิชา</th>
									<th data-width="500" data-field="subject_name" data-sortable="true">ชื่อรายวิชา</th>
                                    <th data-align="center" data-width="300" data-field="operate" data-search-formatter="false" data-formatter="operateFormatter" data-events="operateEvents">ทำรายการ</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
            </div>
        </main>
	</div>

	<!--Modal Add-->
	<div class="modal fade" id="modal_addnew" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">เพิ่มรายวิชาใหม่ &nbsp;<i class="fa fa-plus" aria-hidden="true"></i></h5>
                    <button type="button" class="close" id="modal_addnew_close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="insertForm">
                        <div class="form-row">
                            <table align="center">
                                <tr height="50">
                                    <td><label for="txtSubjID">รหัสวิชา</label></td>
                                    <td><input type="text" id="txtSubjID" class="form-control mx-sm-3" onkeyup="checkFormAddnew()"></td>
                                </tr>
                                <tr height="50">
                                    <td><label for="txtSubjName">ชื่อรายวิชา</label></td>
                                    <td><textarea id="txtSubjName" class="form-control mx-sm-3" onkeyup="checkFormAddnew()"></textarea></td>
                                </tr>
                            </table>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" id="modal_addnew_btnclose">ปิด</button>
                    <button type="button" id="btnSaveAddNew" class="btn btn-primary" disabled>บันทึก</button>
                </div>
            </div>
        </div>
    </div>


    <!--Modal Edit-->
    <div class="modal fade" id="modal_edit" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">แก้ไขรายวิชา &nbsp;<i class="fa fa-edit" aria-hidden="true"></i></h5>
                    <button type="button" class="close" id="modal_edit_close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="insertForm">
                        <div class="form-row">
                            <table align="center">
                                <tr height="50">
                                    <td><label for="txtSubjID">รหัสวิชา</label></td>
                                    <td><input type="text" id="txtSubjID" class="form-control mx-sm-3" onkeyup="checkFormEdit()"></td>
                                </tr>
                                <tr height="50">
                                    <td><label for="txtSubjName">ชื่อรายวิชา</label></td>
                                    <td><textarea id="txtSubjName" class="form-control mx-sm-3" onkeyup="checkFormEdit()"></textarea></td>
                                </tr>
                            </table>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" id="modal_edit_btnclose">ปิด</button>
                    <button type="button" id="btnSaveEdit" class="btn btn-primary">บันทึก</button>
                </div>
            </div>
        </div>
    </div>

    <div id="toolbar">
		<table>
			<tr>
				<td>
					<div class="form-group mx-sm-0 mb-0">
						<button type="button" id="button_add" class="btn btn-primary mb-2">
							เพิ่มรายวิชา <i style="margin-left: 10px;" class="fa fa-plus" aria-hidden="true"></i>
						</button>
					</div>
				</td>
			</tr>
		</table>
	</div>


	<?php include 'lib/script_cdn.php';?>
	<script src="lib/manage_subject.js"></script>

</body>
</html>