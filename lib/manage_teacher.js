var $table = $("#table");
var $tableDegree = $("#tableDegree");

$(document).on('show.bs.modal', '.modal', function (event) {

	var zIndex = 1040 + (10 * $('.modal:visible').length);

	$(this).css('z-index', zIndex);

	setTimeout(function () {
		$('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
	}, 0);
});

function operateFormatter(value, row, index) {
	return [
		'<button class="btn btn-dark" id="degree" title="Degree">วุฒิ\xa0\xa0<i style="font-size: 16px;" class="fas fa-graduation-cap"></i></button>',

		'<span id="symbol10072" style="font-size: 20px; margin-left: 10px;"><b>&#10072;</b></span>',

		'<button class="btn btn-warning" style="margin-left: 10px;" id="edit" title="Edit">แก้ไข\xa0\xa0<i style="font-size: 16px;" class="fas fa-edit"></i></button>',

		'<span id="symbol10072" style="font-size: 20px; margin-left: 10px;"><b>&#10072;</b></span>',

		'<button class="btn btn-danger" style="margin-left: 10px;" id="remove" title="Remove">ลบ\xa0\xa0<i style="font-size: 16px;" class="fas fa-trash"></i></button>',

		].join('')
}

function degreeFormatter(value, row, index) {
	return [
		value + '<a href="javascript:void(0)" style="margin-left: 10px;" id="degreeEdit">แก้ไข\xa0\xa0<i style="font-size: 14px;" class="fas fa-edit"></i></a>',

		'<span id="symbol10072" style="font-size: 20px; margin-left: 10px;">&#10072;</span>',

		'<a href="javascript:void(0)" style="margin-left: 10px; color: #dc3545;" id="degreeDelete">ลบ\xa0\xa0<i style="font-size: 14px;" class="fas fa-trash"></i></a>',	

	].join('')
}

window.degreeEvents = {
	'click #degreeEdit': function (e, value, row, index) {
		$("#modal_editDegree").modal("show");
		$("#modal_editDegree #txtDegreeID").val(row.ID);
		$("#modal_editDegree #txtTeacherID").val(row.t_id);
		$("#modal_editDegree #txtDegree").val(row.d_name);
	},

	'click #degreeDelete': function (e, value, row, index) {
		var degree_id = row.ID;

		Swal.fire({
			title: 'Are you sure ?',
			text: 'คุณต้องการลบข้อมูลวุฒิการศึกษานี้ ?',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Comfirm',
		}).then((result) => {
			if (result.isConfirmed) {
				$.ajax({
					url: 'controller/delete_teachers_degree.php',
					method: 'POST',
					data: {ID: degree_id},

					success: function(response) {
						if (response == "true") {
							Swal.fire({
								icon: 'success',
								title: 'ดำเนินการสำเร็จ !',
								text: 'ลบข้อมูลวุฒิการศึกษาเรียบร้อยแล้ว !',
								width: 700,
								timer: 5000
							})

							$tableDegree.bootstrapTable('refresh')

						} else {
							Swal.fire({
								icon: 'error',
								title: 'ไม่สามารถลบข้อมูลได้ !',
								text: 'อาจเกิดข้อผิดพลาดจากข้อมูลนำเข้า หรือข้อผิดพลาดจากฐานข้อมูล',
								width: 700,
								timer: 5000
							})
						}
					},
					error: function(error) {
						console.log(error);
					}
				});	
			}
		})
	}
}

window.operateEvents = {
	'click #edit': function (e, value, row, index) {
		$("#modal_edit").modal("show");
		$("#modal_edit #txtID").val(row.ID);
		$("#modal_edit #txtPrefix").val(row.prefix);
		$("#modal_edit #txtTeacherName").val(row.fullname);
	},

	'click #remove': function (e, value, row, index) {

		var teacher_id = row.ID;

		Swal.fire({
			title: 'Are you sure ?',
			text: 'คุณต้องการลบข้อมูลอาจารย์ท่านนี้ ?',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Comfirm',
		}).then((result) => {
			if (result.isConfirmed) {
				$.ajax({
					url: 'controller/delete_teachers.php',
					method: 'POST',
					data: {ID: teacher_id},

					success: function(response) {
						if (response == "true") {
							Swal.fire({
								icon: 'success',
								title: 'ดำเนินการสำเร็จ !',
								text: 'ลบข้อมูลอาจารย์เรียบร้อยแล้ว !',
								width: 700,
								timer: 5000
							})

							$table.bootstrapTable('refresh')

						} else {
							Swal.fire({
								icon: 'error',
								title: 'ไม่สามารถลบข้อมูลได้ !',
								text: 'อาจเกิดข้อผิดพลาดจากข้อมูลนำเข้า หรือข้อผิดพลาดจากฐานข้อมูล',
								width: 700,
								timer: 5000
							})
						}
					},
					error: function(error) {
						console.log(error);
					}
				});	
			}
		})
	},

	'click #degree': function (e, value, row, index) {
		$("#modalDegreeList").modal("show");
		$("#modalDegreeList #txtTeacherID").val(row.ID);

		$tableDegree.bootstrapTable('refreshOptions', {url: 'controller/get_data_teachers_degree.php?t_id=' + row.ID})
	}
}

function checkFormAddnew() {

	if ($("#modal_addnew #txtPrefix").val().length > 0 && $("#modal_addnew #txtTeacherName").val().length > 0) {

		$("#btnSaveAddNew").attr("disabled", false)

	} else {

		$("#btnSaveAddNew").attr("disabled", true)

	}
}

function checkFormEdit() {

	if ($("#modal_edit #txtPrefix").val().length > 0 && $("#modal_edit #txtTeacherName").val().length > 0) {

		$("#btnSaveEdit").attr("disabled", false)

	} else {

		$("#btnSaveEdit").attr("disabled", true)

	}
}

function checkFormDegreeAdd() {

	if ($("#modal_addDegree #txtDegree").val().length > 0) {

		$("#btnSaveAddDegree").attr("disabled", false)

	} else {

		$("#btnSaveAddDegree").attr("disabled", true)

	}
}

function checkFormDegreeEdit() {

	if ($("#modal_editDegree #txtDegree").val().length > 0) {

		$("#btnSaveEditDegree").attr("disabled", false)

	} else {

		$("#btnSaveEditDegree").attr("disabled", true)

	}
}


$(document).ready(function () {

	if (localStorage.getItem("user_id") == null || localStorage.getItem("user_name") == null) {

		window.location = "login.php";

		localStorage.removeItem("user_id");
		localStorage.removeItem("user_name");
	}

	$("#close-sidebar").click(function () {
		$(".page-wrapper").removeClass("toggled");
	});

	$("#show-sidebar").click(function () {
		$(".page-wrapper").addClass("toggled");
	});

	$("#button_add").click(function () {
		$("#modal_addnew").modal("show");
	});

	$("#button_addDegree").click(function () {
		$("#modal_addDegree").modal("show");
		$("#modal_addDegree #txtTeacherID").val($("#modalDegreeList #txtTeacherID").val());
	});

	$("#modal_addnew_close").click(function () {
		$("#modal_addnew").modal("hide");
		$("#modal_addnew #txtPrefix").val(null);
		$("#modal_addnew #txtTeacherName").val(null);
		$("#btnSaveAddNew").attr("disabled", true);
	});

	$("#modal_addnew_btnclose").click(function () {
		$("#modal_addnew").modal("hide");
		$("#modal_addnew #txtPrefix").val(null);
		$("#modal_addnew #txtTeacherName").val(null);
		$("#btnSaveAddNew").attr("disabled", true);
	});

	$("#modal_edit_close").click(function () {
		$("#modal_edit").modal("hide");
		$("#modal_edit #txtPrefix").val(null);
		$("#modal_edit #txtTeacherName").val(null);
		$("#btnSaveEdit").attr("disabled", false);
	});

	$("#modal_edit_btnclose").click(function () {
		$("#modal_edit").modal("hide");
		$("#modal_edit #txtPrefix").val(null);
		$("#modal_edit #txtTeacherName").val(null);
		$("#btnSaveEdit").attr("disabled", false);
	});

	$("#modalDegreeListclose").click(function () {
		$("#modalDegreeList").modal("hide");
	});

	$("#modalDegreeListBtnclose").click(function () {
		$("#modalDegreeList").modal("hide");
	});

	$("#modal_editDegree_close").click(function () {
		$("#modal_editDegree").modal("hide");
	});

	$("#modal_editDegree_btnclose").click(function () {
		$("#modal_editDegree").modal("hide");
	});

	$("#modal_addDegree_close").click(function () {
		$("#modal_addDegree").modal("hide");
		$("#modal_addDegree #txtDegree").val(null);
		$("#btnSaveAddDegree").attr("disabled", true);
	});

	$("#modal_addDegree_btnclose").click(function () {
		$("#modal_addDegree").modal("hide");
		$("#modal_addDegree #txtDegree").val(null);
		$("#btnSaveAddDegree").attr("disabled", true);
	});


	$("#btnSaveAddNew").click(function () {

		var _prefix = $("#modal_addnew #txtPrefix").val();
		var _fullname = $("#modal_addnew #txtTeacherName").val();

		Swal.fire({
			title: 'Are you sure ?',
			text: 'คุณต้องการเพิ่มข้อมูลอาจารย์ท่านนี้ ?',
			icon: 'question',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Comfirm',
		}).then((result) => {
			if (result.isConfirmed) {
				$.ajax({
					url: 'controller/insert_teachers.php',
					method: 'POST',
					data: {
						prefix: _prefix,
						fullname: _fullname,
					},

					success: function(response) {
						if (response == "true") {
							Swal.fire({
								icon: 'success',
								title: 'ดำเนินการสำเร็จ !',
								text: 'เพิ่มข้อมูลอาจารย์เรียบร้อยแล้ว !',
								width: 700,
								timer: 5000
							})

							$("#modal_addnew").modal("hide");
							$("#modal_addnew #txtPrefix").val(null);
							$("#modal_addnew #txtTeacherName").val(null);
							$("#btnSaveAddNew").attr("disabled", true);

							$table.bootstrapTable('refresh')

						} else {
							Swal.fire({
								icon: 'error',
								title: 'ไม่สามารถเพิ่มข้อมูลได้ !',
								text: 'อาจเกิดข้อผิดพลาดจากข้อมูลนำเข้า หรือข้อผิดพลาดจากฐานข้อมูล',
								width: 700,
								timer: 5000
							})
						}
					},
					error: function(error) {
						console.log(error);
					}
				});
			}
		})
	});

	$("#btnSaveAddDegree").click(function () {

		var _tid = $("#modal_addDegree #txtTeacherID").val();
		var _dname = $("#modal_addDegree #txtDegree").val();

		Swal.fire({
			title: 'Are you sure ?',
			text: 'คุณต้องการเพิ่มข้อมูลวุฒิการศึกษานี้ ?',
			icon: 'question',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Comfirm',
		}).then((result) => {
			if (result.isConfirmed) {
				$.ajax({
					url: 'controller/insert_teachers_degree.php',
					method: 'POST',
					data: {
						t_id: _tid,
						d_name: _dname,
					},

					success: function(response) {
						if (response == "true") {
							Swal.fire({
								icon: 'success',
								title: 'ดำเนินการสำเร็จ !',
								text: 'เพิ่มข้อมูลวุฒิการศึกษาเรียบร้อยแล้ว !',
								width: 700,
								timer: 5000
							})

							$("#modal_addDegree").modal("hide");
							$("#modal_addDegree #txtTeacherID").val(null);
							$("#modal_addDegree #txtDegree").val(null);
							$("#btnSaveAddDegree").attr("disabled", true);

							$tableDegree.bootstrapTable('refresh')

						} else {
							Swal.fire({
								icon: 'error',
								title: 'ไม่สามารถเพิ่มข้อมูลได้ !',
								text: 'อาจเกิดข้อผิดพลาดจากข้อมูลนำเข้า หรือข้อผิดพลาดจากฐานข้อมูล',
								width: 700,
								timer: 5000
							})
						}
					},
					error: function(error) {
						console.log(error);
					}
				});
			}
		})
	});

	$("#btnSaveEdit").click(function () {

		var _prefix = $("#modal_edit #txtPrefix").val();
		var _fullname = $("#modal_edit #txtTeacherName").val();
		var _id = $("#modal_edit #txtID").val();

		Swal.fire({
			title: 'Are you sure ?',
			text: 'คุณต้องการแก้ไขข้อมูลอาจารย์ท่านนี้ ?',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Comfirm',
		}).then((result) => {
			if (result.isConfirmed) {
				$.ajax({
					url: 'controller/update_teachers.php',
					method: 'POST',
					data: {
						ID: _id,
						prefix: _prefix,
						fullname: _fullname,
					},

					success: function(response) {
						if (response == "true") {
							Swal.fire({
								icon: 'success',
								title: 'ดำเนินการสำเร็จ !',
								text: 'แก้ไขข้อมูลอาจารย์เรียบร้อยแล้ว !',
								width: 700,
								timer: 5000
							})

							$("#modal_edit").modal("hide");
							$("#modal_edit #txtPrefix").val(null);
							$("#modal_edit #txtTeacherName").val(null);
							$("#btnSaveEdit").attr("disabled", false);

							$table.bootstrapTable('refresh')

						} else {
							Swal.fire({
								icon: 'error',
								title: 'ไม่สามารถแก้ไขข้อมูลได้ !',
								text: 'อาจเกิดข้อผิดพลาดจากข้อมูลนำเข้า หรือข้อผิดพลาดจากฐานข้อมูล',
								width: 700,
								timer: 5000
							})
						}
					},
					error: function(error) {
						console.log(error);
					}
				});
			}
		})
	});

	$("#btnSaveEditDegree").click(function () {

		var _id = $("#modal_editDegree #txtDegreeID").val();
		var _tid = $("#modal_editDegree #txtTeacherID").val();
		var _dname = $("#modal_editDegree #txtDegree").val();

		Swal.fire({
			title: 'Are you sure ?',
			text: 'คุณต้องการแก้ไขข้อมูลวุฒิการศึกษานี้ ใช่หรือไม่ ?',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Comfirm',
		}).then((result) => {
			if (result.isConfirmed) {
				$.ajax({
					url: 'controller/update_teachers_degree.php',
					method: 'POST',
					data: {
						ID: _id,
						t_id: _tid,
						d_name: _dname,
					},

					success: function(response) {
						if (response == "true") {
							Swal.fire({
								icon: 'success',
								title: 'ดำเนินการสำเร็จ !',
								text: 'แก้ไขข้อมูลวุฒิการศึกษาเรียบร้อยแล้ว !',
								width: 700,
								timer: 5000
							})

							$("#modal_editDegree").modal("hide");
							$("#btnSaveEditDegree").attr("disabled", false);

							$tableDegree.bootstrapTable('refresh')

						} else {
							Swal.fire({
								icon: 'error',
								title: 'ไม่สามารถแก้ไขข้อมูลได้ !',
								text: 'อาจเกิดข้อผิดพลาดจากข้อมูลนำเข้า หรือข้อผิดพลาดจากฐานข้อมูล',
								width: 700,
								timer: 5000
							})
						}
					},
					error: function(error) {
						console.log(error);
					}
				});
			}
		})
	});

	$("#btn-logout").click(function () {
		Swal.fire({
			title: 'Are you sure ?',
			text: 'คุณต้องการออกจากระบบ ?',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Comfirm',
		}).then((result) => {
			if (result.isConfirmed) {
				localStorage.removeItem("user_id");
				localStorage.removeItem("user_name");

				window.location = "login.php";
			}
		})
	});

	$("#adminName").html(localStorage.getItem("user_name"));
});