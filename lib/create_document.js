$(document).ready(function () {

	if (localStorage.getItem("user_id") == null || localStorage.getItem("user_name") == null) {

		window.location = "login.php";

		localStorage.removeItem("user_id");
		localStorage.removeItem("user_name");
	}

	$("#close-sidebar").click(function () {
		$(".page-wrapper").removeClass("toggled");
	});

	$("#show-sidebar").click(function () {
		$(".page-wrapper").addClass("toggled");
	});

	$("#btn-logout").click(function () {
		Swal.fire({
			title: 'Are you sure ?',
			text: 'คุณต้องการออกจากระบบ ?',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Comfirm',
		}).then((result) => {
			if (result.isConfirmed) {
				localStorage.removeItem("user_id");
				localStorage.removeItem("user_name");

				window.location = "login.php";
			}
		})
	});

	$("#adminName").html(localStorage.getItem("user_name"));
});