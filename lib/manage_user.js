var $table = $("#table");

$(document).on('show.bs.modal', '.modal', function (event) {

	var zIndex = 1040 + (10 * $('.modal:visible').length);

	$(this).css('z-index', zIndex);

	setTimeout(function () {
		$('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
	}, 0);
});

function nameFormatter(value, row, index) {
	return row.first_name + "\xa0\xa0" + row.last_name
}

function operateFormatter(value, row, index) {
	return [
		'<button class="btn btn-danger" style="margin-left: 10px;" id="remove" title="Remove">ลบ\xa0\xa0<i style="font-size: 16px;" class="fas fa-trash"></i></button>'
		].join('')
}

window.operateEvents = {
	'click #remove': function (e, value, row, index) {
		var user_id = row.id;

		Swal.fire({
			title: 'Are you sure ?',
			text: 'คุณต้องการลบข้อมูลผู้ใช้ท่านนี้ ?',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Comfirm',
		}).then((result) => {
			if (result.isConfirmed) {
				$.ajax({
					url: 'controller/delete_users.php',
					method: 'POST',
					data: {user_id: user_id},

					success: function(response) {
						if (response == "true") {
							Swal.fire({
								icon: 'success',
								title: 'ดำเนินการสำเร็จ !',
								text: 'ลบข้อมูลผู้ใช้เรียบร้อยแล้ว !',
								width: 700,
								timer: 5000
							})

							$table.bootstrapTable('refresh')

						} else {
							Swal.fire({
								icon: 'error',
								title: 'ไม่สามารถลบข้อมูลได้ !',
								text: 'อาจเกิดข้อผิดพลาดจากข้อมูลนำเข้า หรือข้อผิดพลาดจากฐานข้อมูล',
								width: 700,
								timer: 5000
							})
						}
					},
					error: function(error) {
						console.log(error);
					}
				});	
			}
		})
	}
}

function checkFormUserAdd() {

	if ($("#modal_addUser #txtUserID").val().length > 0) {

		$("#btnSaveAddUser").attr("disabled", false)

	} else {

		$("#btnSaveAddUser").attr("disabled", true)

	}
}

$(document).ready(function () {

	if (localStorage.getItem("user_id") == null || localStorage.getItem("user_name") == null) {

		window.location = "login.php";

		localStorage.removeItem("user_id");
		localStorage.removeItem("user_name");
	}

	$("#btnAddnew").click(function () {
		$("#modal_addUser").modal("show");
	});

	$("#modal_addUser_close").click(function () {
		$("#modal_addUser").modal("hide");
		$("#modal_addUser #txtUserID").val(null);
		$("#btnSaveAddUser").attr("disabled", true);
	});

	$("#modal_addUser_btnclose").click(function () {
		$("#modal_addUser").modal("hide");
		$("#modal_addUser #txtUserID").val(null);
		$("#btnSaveAddUser").attr("disabled", true);
	});

	$("#txtUserID").click(function() {
		$.ajax({
			url: 'controller/get_data_users_autocomplete.php',
			contentType: 'application/json',
			headers: { "Content-Type": "application/x-www-form-urlencoded" },
			success: function (response) {

			    $( function() {

				    $("#txtUserID").autocomplete({
						source: response
					});

				    $("#txtUserID").autocomplete("option", "appendTo", "#modal_addUser");
	  			});
			},
			error: function (error) {
				console.log(error);
			}
		});
	});

	$("#btnSaveAddUser").click(function() {
		var user_id = $("#txtUserID").val().substring(0, 6);

		$.ajax({
			url: 'controller/get_data_sutpersonel.php',
			contentType: 'application/json',
			headers: { "Content-Type": "application/x-www-form-urlencoded" },
			success: function (response) {
				var validate = false;
				for (var i = 0; i < response.length; i++) {
					if (user_id == response[i]) {
						validate = true;
					}
				}

				if (validate == true) {
					$.ajax({
						url: 'controller/get_data_user_id.php',
						contentType: 'application/json',
						headers: { "Content-Type": "application/x-www-form-urlencoded" },
						success: function (response) {
							var check = true;

							for (var i = 0; i < response.length; i++) {
								if (user_id == response[i]) {
									check = false;
								}
							}

							if (check == false) {
								Swal.fire({
									icon: 'error',
									title: 'ไม่สามารถเพิ่มข้อผู้ใช้ได้ !',
									text: 'ผู้ใช้นี้ มีอยู่ในระบบแล้ว',
									width: 700,
									timer: 5000
								})
							} else {
								Swal.fire({
									title: 'Are you sure ?',
									text: 'คุณต้องการเพิ่มข้อมูลผู้ใช้ท่านนี้ ?',
									icon: 'question',
									showCancelButton: true,
									confirmButtonColor: '#3085d6',
									cancelButtonColor: '#d33',
									confirmButtonText: 'Comfirm',
								}).then((result) => {
									if (result.isConfirmed) {
										$.ajax({
											url: 'controller/insert_users.php',
											method: 'POST',
											data: {user_id: user_id},

											success: function(response) {
												if (response == "true") {
													Swal.fire({
														icon: 'success',
														title: 'ดำเนินการสำเร็จ !',
														text: 'เพิ่มข้อมูลผู้ใช้เรียบร้อยแล้ว !',
														width: 700,
														timer: 5000
													})

													$table.bootstrapTable('refresh');

													$("#modal_addUser").modal("hide");
													$("#modal_addUser #txtUserID").val(null);
													$("#btnSaveAddUser").attr("disabled", true);

												} else {
													Swal.fire({
														icon: 'error',
														title: 'ไม่สามารถเพิ่มข้อมูลได้ !',
														text: 'อาจเกิดข้อผิดพลาดจากข้อมูลนำเข้า หรือข้อผิดพลาดจากฐานข้อมูล',
														width: 700,
														timer: 5000
													})
												}
											},
											error: function(error) {
												console.log(error);
											}
										});
									}
								})
							}
						},
						error: function (error) {
							console.log(error);
						}
					});

				} else {
					Swal.fire({
						icon: 'error',
						title: 'ไม่สามารถเพิ่มข้อผู้ใช้ได้ !',
						text: 'ไม่มีผู้ใช้นี้ในฐานข้อมูลบุคลากร หรือกรอกข้อมูลไม่ถูกต้อง ควรกรอกข้อมูลตัวอย่างเช่น 123456 นายใจดี รักชาติ หรือ 123456',
						width: 700,
						timer: 5000
					})
				}
			},
			error: function (error) {
				console.log(error);
			}
		});
	});

	$("#btn-logout").click(function () {
		Swal.fire({
			title: 'Are you sure ?',
			text: 'คุณต้องการออกจากระบบ ?',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Comfirm',
		}).then((result) => {
			if (result.isConfirmed) {
				localStorage.removeItem("user_id");
				localStorage.removeItem("user_name");

				window.location = "login.php";
			}
		})
	});

	$("#adminName").html(localStorage.getItem("user_name"));

});