$(document).ready(function () {

    $("#txtUsername").keyup(function (usernameEvent) {
        if (usernameEvent.keyCode === 13) {
            $("#btnLogin").click();
        }
    });

    $("#txtPassword").keyup(function (passwordEvent) {
        if (passwordEvent.keyCode === 13) {
            $("#btnLogin").click();
        }
    });

    $('#btnLogin').click(function () {

        if ($('#txtUsername').val() == '' || $('#txtPassword').val() == '') {
            Swal.fire({
                icon: 'error',
                title: 'ไม่สามารถเข้าสู่ระบบได้ !',
                text: 'กรอกข้อมูล Username และ Password ให้ครบถ้วน',
                width: 700,
                timer: 5000
            })
        }
        else {

            $(".loading-icon").removeClass("hide");
            $(".btn").attr("disabled", true);
            $(".btn-txt").text("Loading...");

            $.ajax({
                url: '//smart.sut.ac.th/SUTSmsrtApp/auth.php',
                method: 'POST',
                headers: { "Content-Type": "application/x-www-form-urlencoded" },
                contentType: 'application/json',
                data: {
                    username: $('#txtUsername').val(),
                    password: $('#txtPassword').val(),
                    grant_type: 'password'
                },

                success: function (response) {

                    var psersonel_id = response.instance.id;

                    if (psersonel_id != null) {

                        $.ajax({
                            url: 'controller/validate_user.php',
                            method: 'POST',
                            headers: { "Content-Type": "application/x-www-form-urlencoded" },
                            contentType: 'application/json',
                            data: {user_id: psersonel_id},

                            success: function(response) {
                                if (response.id != null) {

                                    localStorage.setItem("user_name", response.firstname + " " + response.lastname);
                                    localStorage.setItem("user_id", response.id);

                                    window.location = "manage_teacher.php";
                                } else{
                                   Swal.fire({
                                        icon: 'error',
                                        title: 'ไม่สามารถเข้าสู่ระบบได้ !',
                                        text: 'Username หรือ Password ไม่ถูกต้อง',
                                        width: 700,
                                        timer: 5000
                                    })
                                }
                            },
                            error: function(error) {
                                console.log(error);
                            }
                        });                    
                    }
                    else {
                        Swal.fire({
                            icon: 'error',
                            title: 'ไม่สามารถเข้าสู่ระบบได้ !',
                            text: 'Username หรือ Password ไม่ถูกต้อง',
                            width: 700,
                            timer: 5000
                        })

                        $(".loading-icon").addClass("hide");
                        $(".btn").attr("disabled", false);
                        $(".btn-txt").text("Login");

                        $('#txtUsername').val(null);
                        $('#txtPassword').val(null);
                    }
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    });
});

/*$.ajax({
    url: 'controller/validate_user.php',
    method: 'POST',
    headers: { "Content-Type": "application/x-www-form-urlencoded" },
    contentType: 'application/json',
    data: {user_id: psersonel_id},

    success: function(response) {

    },
    error: function(error) {

    }
});*/