var $table = $("#table");
var $tableDegree = $("#tableSubjectDetail");

$(document).on('show.bs.modal', '.modal', function (event) {

	var zIndex = 1040 + (10 * $('.modal:visible').length);

	$(this).css('z-index', zIndex);

	setTimeout(function () {
		$('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
	}, 0);
});

function operateFormatter(value, row, index) {
	return [
		'<button class="btn btn-dark" id="teacher" title="teacher">อาจารย์\xa0\xa0<i style="font-size: 16px;" class="fas fa-graduation-cap"></i></button>',

		'<span id="symbol10072" style="font-size: 20px; margin-left: 10px;"><b>&#10072;</b></span>',

		'<button class="btn btn-warning" style="margin-left: 10px;" id="edit" title="Edit">แก้ไข\xa0\xa0<i style="font-size: 16px;" class="fas fa-edit"></i></button>',

		'<span id="symbol10072" style="font-size: 20px; margin-left: 10px;"><b>&#10072;</b></span>',

		'<button class="btn btn-danger" style="margin-left: 10px;" id="remove" title="Remove">ลบ\xa0\xa0<i style="font-size: 16px;" class="fas fa-trash"></i></button>',

		].join('')
}

window.operateEvents = {
	'click #edit': function (e, value, row, index) {
		$("#modal_edit").modal("show");
		$("#modal_edit #txtSubjID").val(row.subject_id);
		$("#modal_edit #txtSubjName").val(row.subject_name);
	},

	'click #remove': function (e, value, row, index) {
		var subject_id = row.subject_id;

		Swal.fire({
			title: 'Are you sure ?',
			text: 'คุณต้องการลบข้อมูลรายวิชานี้ ?',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Comfirm',
		}).then((result) => {
			if (result.isConfirmed) {
				$.ajax({
					url: 'controller/delete_allsubject.php',
					method: 'POST',
					data: {subject_id: subject_id},

					success: function(response) {
						if (response == "true") {
							Swal.fire({
								icon: 'success',
								title: 'ดำเนินการสำเร็จ !',
								text: 'ลบข้อมูลรายวิชาเรียบร้อยแล้ว !',
								width: 700,
								timer: 5000
							})

							$table.bootstrapTable('refresh')

						} else {
							Swal.fire({
								icon: 'error',
								title: 'ไม่สามารถลบข้อมูลได้ !',
								text: 'อาจเกิดข้อผิดพลาดจากข้อมูลนำเข้า หรือข้อผิดพลาดจากฐานข้อมูล',
								width: 700,
								timer: 5000
							})
						}
					},
					error: function(error) {
						console.log(error);
					}
				});	
			}
		})
	},

	'click #teacher': function (e, value, row, index) {
		alert("teacher");
	}
}

function checkFormAddnew() {

	if ($("#modal_addnew #txtSubjID").val().length > 0 && $("#modal_addnew #txtSubjName").val().length > 0) {

		$("#btnSaveAddNew").attr("disabled", false)

	} else {

		$("#btnSaveAddNew").attr("disabled", true)

	}
}

function checkFormEdit() {

	if ($("#modal_edit #txtSubjID").val().length > 0 && $("#modal_edit #txtSubjName").val().length > 0) {

		$("#btnSaveEdit").attr("disabled", false)

	} else {

		$("#btnSaveEdit").attr("disabled", true)

	}
}

$(document).ready(function () {

	if (localStorage.getItem("user_id") == null || localStorage.getItem("user_name") == null) {

		window.location = "login.php";

		localStorage.removeItem("user_id");
		localStorage.removeItem("user_name");
	}

	$("#btnSaveAddNew").click(function () {

		var subject_id = $("#modal_addnew #txtSubjID").val();
		var subject_name = $("#modal_addnew #txtSubjName").val();

		if(subject_id == null || subject_name == null || subject_id == "" || subject_name == "") {
			Swal.fire({
				icon: 'error',
				title: 'ไม่สามารถเพิ่มข้อมูลได้ !',
				text: 'คุณกรอกข้อมูลไม่ครบถ้วน กรุณาลองใหม่อีกครั้ง',
				width: 700,
				timer: 5000
			})
		} else {
			Swal.fire({
				title: 'Are you sure ?',
				text: 'คุณต้องการเพิ่มข้อมูลรายวิชานี้ ?',
				icon: 'question',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Comfirm',
			}).then((result) => {
				if (result.isConfirmed) {
					$.ajax({
						url: 'controller/insert_allsubject.php',
						method: 'POST',
						data: {
							subject_id: subject_id,
							subject_name: subject_name,
						},

						success: function(response) {
							if (response == "true") {
								Swal.fire({
									icon: 'success',
									title: 'ดำเนินการสำเร็จ !',
									text: 'เพิ่มข้อมูลรายวิชาเรียบร้อยแล้ว !',
									width: 700,
									timer: 5000
								})

								$("#modal_addnew").modal("hide");
								$("#modal_addnew #txtSubjID").val(null);
								$("#modal_addnew #txtSubjName").css("height", "100px");
								$("#modal_addnew #txtSubjName").val(null);
								$("#btnSaveAddNew").attr("disabled", true);

								$table.bootstrapTable('refresh')

							} else {
								Swal.fire({
									icon: 'error',
									title: 'ไม่สามารถเพิ่มข้อมูลได้ !',
									text: 'รายวิชานี้อาจมีในระบบแล้ว หรือข้อมูลนำเข้าผิดพลาดกรุณาตรวจสอบข้อมูลอีกครั้ง',
									width: 700,
									timer: 5000
								})
							}
						},
						error: function(error) {
							console.log(error);
						}
					});
				}
			})
		}
	});

	$("#btnSaveEdit").click(function () {

		var subject_id = $("#modal_edit #txtSubjID").val();
		var subject_name = $("#modal_edit #txtSubjName").val();

		if(subject_id == null || subject_name == null || subject_id == "" || subject_name == "") {
			Swal.fire({
				icon: 'error',
				title: 'ไม่สามารถแก้ไขข้อมูลได้ !',
				text: 'คุณกรอกข้อมูลไม่ครบถ้วน กรุณาลองใหม่อีกครั้ง',
				width: 700,
				timer: 5000
			})
		} else {
			Swal.fire({
				title: 'Are you sure ?',
				text: 'คุณต้องการแก้ไขข้อมูลรายวิชานี้ ?',
				icon: 'question',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Comfirm',
			}).then((result) => {
				if (result.isConfirmed) {
					$.ajax({
						url: 'controller/update_allsubject.php',
						method: 'POST',
						data: {
							subject_id: subject_id,
							subject_name: subject_name,
						},

						success: function(response) {
							if (response == "true") {
								Swal.fire({
									icon: 'success',
									title: 'ดำเนินการสำเร็จ !',
									text: 'แก้ไขข้อมูลรายวิชาเรียบร้อยแล้ว !',
									width: 700,
									timer: 5000
								})

								$("#modal_edit").modal("hide");
								$("#modal_edit #txtSubjID").val(null);
								$("#modal_edit #txtSubjName").css("height", "100px");
								$("#modal_edit #txtSubjName").val(null);
								$("#btnSaveEdit").attr("disabled", false);

								$table.bootstrapTable('refresh')

							} else {
								Swal.fire({
									icon: 'error',
									title: 'ไม่สามารถแก้ไขข้อมูลได้ !',
									text: 'รายวิชานี้อาจมีในระบบแล้ว หรือข้อมูลนำเข้าผิดพลาดกรุณาตรวจสอบข้อมูลอีกครั้ง',
									width: 700,
									timer: 5000
								})
							}
						},
						error: function(error) {
							console.log(error);
						}
					});
				}
			})
		}
	});

	$("#close-sidebar").click(function () {
		$(".page-wrapper").removeClass("toggled");
	});

	$("#show-sidebar").click(function () {
		$(".page-wrapper").addClass("toggled");
	});

	$("#button_add").click(function () {
		$("#modal_addnew").modal("show");
	});

	$("#modal_addnew_close").click(function () {
		$("#modal_addnew").modal("hide");
		$("#modal_addnew #txtSubjID").val(null);
		$("#modal_addnew #txtSubjName").css("height", "100px");
		$("#modal_addnew #txtSubjName").val(null);
		$("#btnSaveAddNew").attr("disabled", true);
	});

	$("#modal_addnew_btnclose").click(function () {
		$("#modal_addnew").modal("hide");
		$("#modal_addnew #txtSubjID").val(null);
		$("#modal_addnew #txtSubjName").css("height", "100px");
		$("#modal_addnew #txtSubjName").val(null);
		$("#btnSaveAddNew").attr("disabled", true);
	});

	$("#modal_edit_close").click(function () {
		$("#modal_edit").modal("hide");
		$("#modal_edit #txtSubjID").val(null);
		$("#modal_edit #txtSubjName").css("height", "100px");
		$("#modal_edit #txtSubjName").val(null);
		$("#btnSaveEdit").attr("disabled", false);
	});

	$("#modal_edit_btnclose").click(function () {
		$("#modal_edit").modal("hide");
		$("#modal_edit #txtSubjID").val(null);
		$("#modal_edit #txtSubjName").css("height", "100px");
		$("#modal_edit #txtSubjName").val(null);
		$("#btnSaveEdit").attr("disabled", false);
	});

	$("#btn-logout").click(function () {
		Swal.fire({
			title: 'Are you sure ?',
			text: 'คุณต้องการออกจากระบบ ?',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Comfirm',
		}).then((result) => {
			if (result.isConfirmed) {
				localStorage.removeItem("user_id");
				localStorage.removeItem("user_name");

				window.location = "login.php";
			}
		})
	});

	$("#adminName").html(localStorage.getItem("user_name"));
});