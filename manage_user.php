<!DOCTYPE html>
<html>
<head>
	<title>ระบบแต่งตั้งอาจารย์พิเศษ</title>
    <meta charset="UTF-8">

    <?php include 'lib/css_cdn.php';?>
    <link href="lib/manage_user.css" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

</head>
<body>
	<div class="page-wrapper chiller-theme toggled">
		<a href="#" id="show-sidebar" class="btn btn-sm btn-dark" style="width: 100px; height: 50px">
			Console <br />
			<i class="fa fa-sliders"></i>
		</a>
		<nav id="sidebar" class="sidebar-wrapper">
			<div class="sidebar-content">
				<div class="sidebar-brand">
					<a>Console</a>
					<div id="close-sidebar">
						<i class="fa fa-close"></i>
					</div>
				</div>
				<div class="sidebar-header">
					<div class="user-pic">
						<img class="img-responsive img-rounded" src="https://raw.githubusercontent.com/azouaoui-med/pro-sidebar-template/gh-pages/src/img/user.jpg" alt="User picture">
					</div>
					<div class="user-info">
						<span id="adminName" class="user-name"></span>
						<span class="user-role">Administrator</span>
					</div>
				</div>

				<div class="sidebar-menu">
					<ul>
						<li class="header-menu">
							<span>Menu</span>
						</li>
						<li>
                            <a href="manage_teacher.php">
                                <i class="fa fa-graduation-cap"></i>
                                <span>อาจารย์</span>
                            </a>
                        </li>
                        <li>
                            <a href="manage_subject.php">
                                <i class="fa fa-book"></i>
                                <span>รายวิชา</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-inbox"></i>
                                <span>เลขที่คำสั่ง มทส.</span>
                            </a>
                        </li>
                        <li>
                            <a href="create_document.php">
                                <i class="fa fa-file"></i>
                                <span>ออกหนังสือแต่งตั้ง</span>
                            </a>
                        </li>
                        <li>
                            <a href="manage_user.php">
                                <i class="fa fa-user"></i>
                                <span>ผู้ใช้ระบบ</span>
                            </a>
                        </li>
					</ul>
				</div>
			</div>

			<div class="sidebar-footer">
				<a href="#" id="btn-logout">
					<i class="fa fa-power-off"></i>&nbsp;&nbsp;ออกจากระบบ
				</a>
			</div>
		</nav>

		<main class="page-content">
            <div id="main">
                <div class="card" id="card-title">
					<div class="card-body">
						<h2>
							<b style="margin-left: 10px;">ผู้ใช้ระบบ</b>
							<i style="margin-left: 20px;" class="fa fa-user"></i>
						</h2>
					</div>
				</div>

				<div class="card" id="card-table">
					<div class="card-body">
						<table
							id="table"
							data-toggle="table"
							data-toolbar="#toolbar"
							data-search="true"
							data-show-refresh="true"
							data-page-list="[5, 10, 20, 100]"
							data-pagination="true"
							data-pagination-pre-text="Previous"
							data-pagination-next-text="Next"
							data-page-size="10"
							data-url="controller/get_data_users.php">
							<thead class="thead-dark">
								<tr>
									<th data-field="id" data-sortable="true">รหัสบุคลากร</th>
									<th data-formatter="nameFormatter" data-sortable="true">ชื่อ-นามสกุล</th>
									<th data-field="position" data-sortable="true">ตำแหน่ง</th>
                                    <th data-field="email" data-sortable="true">อีเมล</th>
                                    <th data-field="tel_mobile" data-sortable="true">เบอร์โทรศัพท์</th>
									<th data-align="center" data-field="operate" data-search-formatter="false" data-formatter="operateFormatter" data-events="operateEvents">ทำรายการ</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
            </div>
        </main>
	</div>

    <div id="toolbar">
		<table>
			<tr>
				<td>
					<div class="form-group mx-sm-0 mb-0">
						<button type="button" id="btnAddnew" class="btn btn-primary mb-2">
							เพิ่มผู้ใช้ระบบ <i style="margin-left: 10px;" class="fa fa-plus" aria-hidden="true"></i>
						</button>
					</div>
				</td>
			</tr>
		</table>
	</div>

    <!--Modal User Addnew-->
    <div class="modal fade" id="modal_addUser" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">เพิ่มผู้ใช้ระบบ &nbsp;<i class="fa fa-plus" aria-hidden="true"></i></h5>
                    <button type="button" class="close" id="modal_addUser_close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <table align="center">
                            <tr height="50">
                                <td><label for="txtUserID">ผู้ใช้</label></td>
                                <td>
                                    <div class="ui-widget">
                                        <input type="text" id="txtUserID" class="form-control mx-sm-3" onkeyup="checkFormUserAdd()">
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" id="modal_addUser_btnclose">ปิด</button>
                    <button type="button" id="btnSaveAddUser" class="btn btn-primary" disabled>บันทึก</button>
                </div>
            </div>
        </div>
    </div>

	<?php include 'lib/script_cdn.php';?>
	<script src="lib/manage_user.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

</body>
</html>