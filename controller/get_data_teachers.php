<?php

header('Content-Type: application/json');

	$connect = mysqli_connect("localhost", "root", "", "mis_project_db");

	$query = "SELECT * FROM teachers";

	$result = mysqli_query($connect, $query);

	$data = array();

	while ($row = mysqli_fetch_array($result)) {
		$data[] = array(
			'ID'		=>	$row["ID"],
			'prefix'	=>	$row["prefix"],
			'fullname'	=>	$row["fullname"],
		);
	}

	echo json_encode($data, JSON_UNESCAPED_UNICODE);

	$connect -> close();
	
?>
