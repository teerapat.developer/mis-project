<?php

header('Content-Type: application/json');

	$connect = mysqli_connect("localhost", "root", "", "mis_project_db");

	$query = "SELECT *
			  FROM sut_personels
			  INNER JOIN users
			  ON sut_personels.id = users.user_id;";

	$result = mysqli_query($connect, $query);

	$data = array();

	while ($row = mysqli_fetch_array($result)) {
		$data[] = array(
			'id'			=>	$row["id"],
			'first_name'	=>	$row["first_name"],
			'last_name'		=>	$row["last_name"],
			'position'		=>	$row["position"],
			'email'			=>	$row["email"],
			'tel_mobile'	=>	$row["tel_mobile"],
		);
	}

	echo json_encode($data, JSON_UNESCAPED_UNICODE);

	$connect -> close();
	
?>
