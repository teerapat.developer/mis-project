<?php

header('Content-Type: application/json');

	$connect = mysqli_connect("localhost", "root", "", "mis_project_db");

	$query = "SELECT * FROM allsubject";

	$result = mysqli_query($connect, $query);

	$data = array();

	while ($row = mysqli_fetch_array($result)) {
		$data[] = array(
			'subject_id'	=>	$row["subject_id"],
			'subject_name'	=>	$row["subject_name"],
		);
	}

	echo json_encode($data, JSON_UNESCAPED_UNICODE);

	$connect -> close();
	
?>
