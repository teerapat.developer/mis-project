<!DOCTYPE html>
<html>
<head>
	<title>ระบบแต่งตั้งอาจารย์พิเศษ</title>
    <meta charset="UTF-8">

    <?php include 'lib/css_cdn.php';?>
    <link href="lib/create_document.css" rel="stylesheet">

</head>
<body>
	<div class="page-wrapper chiller-theme toggled">
		<a href="#" id="show-sidebar" class="btn btn-sm btn-dark" style="width: 100px; height: 50px">
			Console <br />
			<i class="fa fa-sliders"></i>
		</a>
		<nav id="sidebar" class="sidebar-wrapper">
			<div class="sidebar-content">
				<div class="sidebar-brand">
					<a>Console</a>
					<div id="close-sidebar">
						<i class="fa fa-close"></i>
					</div>
				</div>
				<div class="sidebar-header">
					<div class="user-pic">
						<img class="img-responsive img-rounded" src="https://raw.githubusercontent.com/azouaoui-med/pro-sidebar-template/gh-pages/src/img/user.jpg" alt="User picture">
					</div>
					<div class="user-info">
						<span id="adminName" class="user-name"></span>
						<span class="user-role">Administrator</span>
					</div>
				</div>

				<div class="sidebar-menu">
					<ul>
						<li class="header-menu">
							<span>Menu</span>
						</li>
						<li>
                            <a href="manage_teacher.php">
                                <i class="fa fa-graduation-cap"></i>
                                <span>อาจารย์</span>
                            </a>
                        </li>
                        <li>
                            <a href="manage_subject.php">
                                <i class="fa fa-book"></i>
                                <span>รายวิชา</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-inbox"></i>
                                <span>เลขที่คำสั่ง มทส.</span>
                            </a>
                        </li>
                        <li>
                            <a href="create_document.php">
                                <i class="fa fa-file"></i>
                                <span>ออกหนังสือแต่งตั้ง</span>
                            </a>
                        </li>
                        <li>
                            <a href="manage_user.php">
                                <i class="fa fa-user"></i>
                                <span>ผู้ใช้ระบบ</span>
                            </a>
                        </li>
					</ul>
				</div>
			</div>

			<div class="sidebar-footer">
				<a href="#" id="btn-logout">
					<i class="fa fa-power-off"></i>&nbsp;&nbsp;ออกจากระบบ
				</a>
			</div>
		</nav>

		<main class="page-content">
            <div id="main">
                <div class="card" id="card-title">
					<div class="card-body">
						<h2>
							<b style="margin-left: 10px;">ออกหนังสือแต่งตั้ง</b>
							<i style="margin-left: 20px;" class="fa fa-file-text"></i>
						</h2>
					</div>
				</div>

                <div class="card" id="card-form">
                    <div class="card-body">
                        
                    </div>
                </div>
            </div>
        </main>
	</div>

	<?php include 'lib/script_cdn.php';?>
	<script src="lib/create_document.js"></script>

</body>
</html>