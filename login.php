<!DOCTYPE html>
<html>
<head>
    <title>Login</title>
    <meta charset="UTF-8">

    <!--==========================================================================================================-->
    <link href="lib/login_templete/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" >
    <!--==========================================================================================================-->
    <link href="lib/login_templete/fonts/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!--==========================================================================================================-->
    <link href="lib/login_templete/fonts/iconic/css/material-design-iconic-font.min.css" rel="stylesheet">
    <!--==========================================================================================================-->
    <link href="lib/login_templete/vendor/animate/animate.css" rel="stylesheet">
    <!--==========================================================================================================-->
    <link href="lib/login_templete/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet">
    <!--==========================================================================================================-->
    <link href="lib/login_templete/vendor/animsition/css/animsition.min.css" rel="stylesheet">
    <!--==========================================================================================================-->
    <link href="lib/login_templete/vendor/select2/select2.min.css" rel="stylesheet">
    <!--==========================================================================================================-->
    <link href="lib/login_templete/vendor/daterangepicker/daterangepicker.css" rel="stylesheet">
    <!--==========================================================================================================-->
    <link href="lib/login_templete/css/util.css" rel="stylesheet">
    <!--==========================================================================================================-->
    <link href="lib/login_templete/css/main.css" rel="stylesheet">
    <!--==========================================================================================================-->

    <style>
        btn:disabled {
            opacity: 0.5;
        }

        .hide {
            display: none;
        }
    </style>
</head>

<body class="bg">

    <div class="limiter">
        <div class="container-login100" style="background-image: url('lib/images/tri_bg.png'); background-repeat: no-repeat;">
            <div class="wrap-login100">
                <span class="login100-form-logo">
                    <img src="lib/images/logo.png" width="60" />
                </span>
                <span class="login100-form-title p-b-34 p-t-27">
                    ระบบแต่งตั้งอาจารย์พิเศษ
                </span>

                <div class="wrap-input100 validate-input" data-validate="Enter username">
                    <input class="input100" type="text" id="txtUsername" placeholder="Username">
                    <span class="focus-input100" data-placeholder="&#xf207;"></span>
                </div>

                <div class="wrap-input100 validate-input" data-validate="Enter password">
                    <input class="input100" type="password" id="txtPassword" placeholder="Password">
                    <span class="focus-input100" data-placeholder="&#xf191;"></span>
                </div>

                <div class="contact100-form-checkbox"></div>

                <div class="container-login100-form-btn">
                    <button class="btn login100-form-btn" style="border: none;" id="btnLogin">
                        <i class="loading-icon fa fa-spinner fa-spin hide"></i>
                        <span class="txt">&nbsp;&nbsp;</span>
                        <span class="btn-txt">Login</span>
                        <span class="txt">&nbsp;&nbsp;</span>
                    </button>
                </div>

                <div class="text-center p-t-70"></div>
            </div>
        </div>
    </div>

    <!--==========================================================================================================-->
    <script src="lib/login_templete/vendor/jquery/jquery-3.2.1.min.js"></script>
    <!--==========================================================================================================-->
    <script src="lib/login_templete/vendor/animsition/js/animsition.min.js"></script>
    <!--==========================================================================================================-->
    <script src="lib/login_templete/vendor/bootstrap/js/popper.js"></script>
    <!--==========================================================================================================-->
    <script src="lib/login_templete/vendor/bootstrap/js/popper.min.js"></script>
    <!--==========================================================================================================-->
    <script src="lib/login_templete/vendor/select2/select2.min.js"></script>
    <!--==========================================================================================================-->
    <script src="lib/login_templete/vendor/daterangepicker/moment.min.js"></script>
    <!--==========================================================================================================-->
    <script src="lib/login_templete/vendor/daterangepicker/daterangepicker.js"></script>
    <!--==========================================================================================================-->
    <script src="lib/login_templete/vendor/countdowntime/countdowntime.js"></script>
    <!--==========================================================================================================-->
    <script src="lib/login_templete/js/main.js"></script>
    <!--==========================================================================================================-->
    <script src="lib/login.js"></script>
    <!--==========================================================================================================-->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <!--==========================================================================================================-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!--==========================================================================================================-->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <!--==========================================================================================================-->